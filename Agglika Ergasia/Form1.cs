﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agglika_Ergasia
{
    public partial class Form1 : Form
    {
        string path = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string studentName = SName.Text.Trim();
            string email = SEmail.Text.Trim();
            int age = 0;

            bool ageParsed = Int32.TryParse(SAge.Text.Trim(), out age);

            SName.Text = SEmail.Text = SAge.Text = string.Empty;

            if (!ageParsed || age <= 0)
            {
                MessageBox.Show("Invalid Data: age!");
                return;
            }
            
            if (!email.Contains("@") || !email.Contains(".") || string.IsNullOrEmpty(email))
            {
                MessageBox.Show("Invalid Data: email!");
                return;
            }

            if (string.IsNullOrEmpty(studentName))
            {
                MessageBox.Show("Invalid Data: strudent name!");
                return;
            }

            string file = path + studentName + ".bin";

            if (File.Exists(file))
            {
                MessageBox.Show("Student with name: " + studentName + " already exists!");
                return;
            }

            try
            {
                BinaryWriter writer = new BinaryWriter(new FileStream(file, FileMode.Create));

                writer.Write(age);
                writer.Write(email);

                writer.Close();

                MessageBox.Show("Student: " + studentName + " has been saved!");
                return;
            }
            catch (Exception ex) { MessageBox.Show("Error on saving student: " + studentName + ". Error: " + ex.Message); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string studentName = LName.Text.Trim();
            LName.Text = string.Empty;

            if (string.IsNullOrEmpty(path))
            {
                MessageBox.Show("Internal Error");
                return;
            }

            string file = path + studentName + ".bin";
            string[] files = Directory.GetFiles(path);

            foreach (var f in files)
            {
                if (f == file)
                {
                    try
                    {
                        BinaryReader reader = new BinaryReader(new FileStream(file, FileMode.Open));

                        int age = reader.ReadInt32();
                        string email = reader.ReadString();

                        reader.Close();

                        if (age <= 0 || string.IsNullOrEmpty(email))
                        {
                            MessageBox.Show("Found invalid student data for student with name: " + studentName);
                            return;
                        }

                        MessageBox.Show("Student: " + studentName + "\n Age: " + age + " \n Email: " + email);
                        return;
                    }
                    catch (Exception ex) { MessageBox.Show("Error on loading data from student: " + studentName + ". Error: " + ex.Message); }
                }
            }

            MessageBox.Show("Student not found!");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            path = Directory.GetCurrentDirectory() + "/data/";
        }
    }
}
